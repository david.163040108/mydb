package id.ac.unpas.sab.mydb;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ViewMahasiswaActivity extends AppCompatActivity {
    ListView myListView;
    private List<String> listMhs;
    private String nrp;
    private List<MahasiswaPojo> pojoList;
    protected DatabaseHelper dbHelper;
    protected SQLiteDatabase db;
    protected Cursor cursor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_mahasiswa);
        myListView = (ListView) findViewById(R.id.lsMhs);
        listMhs = new ArrayList<>();
        pojoList = new ArrayList<>();

        dbHelper = new DatabaseHelper(this);
        db = dbHelper.getWritableDatabase();
        try{
            loadDataMahasiswa();
        }catch (Exception e){
            Log.e("Masuk", "->" +e.getMessage() );
        }

        getAllData();
    }

    private void loadDataMahasiswa() {

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, listMhs );
        myListView.setAdapter(adapter);
        myListView.setSelected(true);
        myListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {

                final CharSequence[] dialogitem = {"Update Mahasiswa", "Hapus Mahasiswa"};
                AlertDialog.Builder builder = new AlertDialog.Builder(ViewMahasiswaActivity.this);
                builder.setTitle("Pilihan");
                builder.setItems(dialogitem, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        switch (item){
                            case 0 :
                                String nrpIntent = pojoList.get(position).getNrp();
                                Intent intent = new Intent(getApplication(), UpdateMahasiswaActivity.class);
                                intent.putExtra("nrp", nrpIntent);
                                startActivity(intent);
                                ViewMahasiswaActivity.this.finish();
                                break;
                            case 1 :
                                String nrpHapus = pojoList.get(position).getNrp();
                                 dbHelper.deleteDataMahasiswa(db,  nrpHapus );
                                Intent intent2 = new Intent(getApplication(), ViewMahasiswaActivity.class);
                                startActivity(intent2);
                                finish();
                                break;
                        }
                    }
                });
                builder.create().show();
            }
        });     ((ArrayAdapter)myListView.getAdapter()).notifyDataSetInvalidated();
    }

    private void getAllData() {

        Cursor cursor = dbHelper.getAll(db);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()){
            MahasiswaPojo mhs = new MahasiswaPojo();

            String nrp = cursor.getString(cursor.getColumnIndex(DatabaseHelper.nrp));
            String nama = cursor.getString(cursor.getColumnIndex(DatabaseHelper.nama));
            String prodi = cursor.getString(cursor.getColumnIndex(DatabaseHelper.prodi));

            listMhs.add(nrp +" - "+nama+" - "+prodi);
            mhs.setNrp(nrp);
            mhs.setNama(nama);
            mhs.setProdi(prodi);

            pojoList.add(mhs);
            cursor.moveToNext();
        }

        cursor.close();
    }
}
