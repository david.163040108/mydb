package id.ac.unpas.sab.mydb;


import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class AddMahasiswaActivity extends AppCompatActivity {

    EditText nrp, nama, prodi;
    Button insert;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_mahasiswa);

        nrp = (EditText) findViewById(R.id.nrpMhs);
        nama = (EditText) findViewById(R.id.namaMhs);
        prodi = (EditText) findViewById(R.id.prodi);
        insert = (Button) findViewById(R.id.btnInsert);

        insert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String sNrp = nrp.getText().toString().trim();
                String sNama = nama.getText().toString().trim();
                String sProdi = prodi.getText().toString().trim();

               if (ceKosong(sNrp,sNama,sProdi)) {
                   if (cekNrp(sNrp)) {
                       if (cekNama(sNama)) {
                           if (cekKarakter(sNama)) {
                               DatabaseHelper dbHelper = new DatabaseHelper(AddMahasiswaActivity.this);
                               SQLiteDatabase db = dbHelper.getWritableDatabase();
                               dbHelper.createMahasiswaTable(db);
                               dbHelper.insertDataMahasiswa(db, sNrp, sNama, sProdi);
                               Toast.makeText(AddMahasiswaActivity.this,
                                       "Insert Data Mahasiswa Succes", Toast.LENGTH_LONG).show();

                               nrp.setText("");
                               nama.setText("");
                               prodi.setText("");
                           } else {
                               Toast.makeText(AddMahasiswaActivity.this,
                                       " Inputan tidak boleh ada karakter atau angka.", Toast.LENGTH_LONG).show();

                           }

                       } else {
                           Toast.makeText(AddMahasiswaActivity.this, "inputan nama minimal berisi 3 huruf",
                                   Toast.LENGTH_LONG).show();

                       }
                   } else {
                       Toast.makeText(AddMahasiswaActivity.this, "Inputan NRP tidak boleh kurang atau lebih dari 9 digit",
                               Toast.LENGTH_LONG).show();
                   }
               }else {
                   Toast.makeText(AddMahasiswaActivity.this, "Inputan tidak boleh kosong",
                           Toast.LENGTH_LONG).show();
               }
            }
        });
    }



    public boolean cekNrp(String nrp){
        if (!(nrp.length() >= 9) || !(nrp.length() <= 9)){
            return false;
        }
        return  true;
    }

    public  boolean cekNama(String nama){
        if (!(nama.length() >= 3)){
            return  false;
        }
        return true;
    }

    public boolean cekKarakter(String nama){
        String REGEX = "[^&%$#@!+^~*?<>,.`;:'0-9]+";
        Pattern pattern = Pattern.compile(REGEX);
        Matcher matcher = pattern.matcher(nama);

        if(matcher.matches()){
            return true;
        }
            return false;
    }

    public  boolean ceKosong(String nrp ,String nama, String prodi){
        if (nrp.equals("") || nama.equals("") || prodi.equals("")){
            return false;
        }

        return true;
    }

}
