package id.ac.unpas.sab.mydb;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class UpdateMahasiswaActivity extends AppCompatActivity {

    protected DatabaseHelper dbHelper;
    protected SQLiteDatabase db;
    EditText nrpET, namaET,prodiET;
    Button buttonUpdate;
    protected Cursor cursor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_mahasiswa);
        nrpET = (EditText) findViewById(R.id.nrpUpdate);
        namaET = (EditText) findViewById(R.id.namaMhsUpdate);
        prodiET = (EditText) findViewById(R.id.prodiUpdate);
        buttonUpdate = (Button) findViewById(R.id.btnUpdate);


        //update

        final DatabaseHelper dbHelper = new DatabaseHelper(UpdateMahasiswaActivity.this);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        cursor = db.rawQuery("SELECT * FROM table_mahaiswa WHERE nrp = '" +
                getIntent().getStringExtra("nrp") + "'",null);
        cursor.moveToFirst();
        if (cursor.getCount()>0)
        {
            cursor.moveToPosition(0);
            nrpET.setText(cursor.getString(0).toString());
            namaET.setText(cursor.getString(1).toString());
            prodiET.setText(cursor.getString(2).toString());
        }

        buttonUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String sNrp = nrpET.getText().toString().trim();
                final String sNama = namaET.getText().toString().trim();
                final String sProdi = prodiET.getText().toString().trim();
                SQLiteDatabase db = dbHelper.getWritableDatabase();
                dbHelper.updateDataMahasiswa(db, sNrp,sNama,sProdi);

                Toast.makeText(UpdateMahasiswaActivity.this,
                        "Update Data Mahasiswa Succes", Toast.LENGTH_LONG).show();

                Intent intent = new Intent(UpdateMahasiswaActivity.this, ViewMahasiswaActivity.class);
                startActivity(intent);
                UpdateMahasiswaActivity.this.finish();
            }
        });

    }
}
